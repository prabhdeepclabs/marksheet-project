//
//  ViewController.swift
//  marksheet
//
//  Created by Bhasker on 1/14/15.
//  Copyright (c) 2015 Bhasker. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    // text fields for student information and marks
    
    @IBOutlet var studentInput: [UITextField]!
    @IBOutlet var marks: [UITextField]!
    
    
    var sum = 0
    var avg = 0
    var subjectIndex = 0
    var infoIndex = 0
    var flag = 0
    var i = 0
    
    
    // image view of grade
    
    @IBOutlet weak var myImage: UIImageView!
    
    
    // grade output labels
    
    @IBOutlet weak var grade: UILabel!
    @IBOutlet weak var percent: UILabel!
    @IBOutlet weak var error: UILabel!
    
    
    // student info output labels collection
    
    @IBOutlet var studentOutput: [UILabel]!
    
    
    // hidden labels collection
    
    @IBOutlet var hiddenLabels: [UILabel]!
    
    
    // submit button pressed
    
    @IBAction func calc(sender: AnyObject) {
        
        self.view.endEditing(true) // hide keyboard
        
        // check... is student information filled
        
        while (infoIndex < 3)
        {
            if ( studentInput[infoIndex].text == "") {
                
                flag++
    
            }
            infoIndex++
            
        }
        
        infoIndex = 0  /* clear infoIndex so that it can be              used again */
        
        
        // check... are marks filled
        
        while (subjectIndex < 5)
        {
            if ( marks[subjectIndex].text.toInt() != nil) {
            
                
                sum = sum + marks[subjectIndex].text.toInt()!
                
                println (marks[subjectIndex].text)
                
            } else {// if text box left empty
                
                flag++
                
            }
            subjectIndex++
        }
        
        
        
        
        if (flag != 0) {    // if error
            
            error.text = "Fill all correct"
            
        } else {        // if no error show result
            
            
        
            error.text = "" //show no error
            
            // calculate perecntage and grades
            
            avg = sum/5
            percent.text = "\(avg)"
            
            if avg < 50 {
                
                grade.text = "poor"
                myImage.image = UIImage (named: "poor")
            }
                
            else if avg < 75 {
                
                grade.text = "average"
                myImage.image = UIImage (named: "average")
                
            }
                
            else {
                
                grade.text = "excellent"
                myImage.image = UIImage (named: "excellent")
                
            }
            
            
            // show student information
            
            while (infoIndex < 3)
            {
                studentOutput[infoIndex].text = studentInput[infoIndex].text
                    
                infoIndex++
                
            }

    
            // show labels
            while ( i < 5 ) {
                
                hiddenLabels[i].hidden = false
                i++
            }

        }
        
        
        // clearing all values
        
        sum = 0
        subjectIndex = 0
        infoIndex = 0
        flag = 0
        i=0

    }

    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

